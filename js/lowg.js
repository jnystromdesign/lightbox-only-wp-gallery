(function($){

	// On load… 
	$(function(){
		 
		var $gallery 		= $('.gallery');
		var $galleryItems 	= $('.gallery-item');
		
		// Fetch Description and add it as an anchor title.
		$.each($galleryItems, function(i,v){
			var description 	= $(this).find('dd').text().trim();
			var $itemLink 		= $(this).find('a');
			$itemLink.attr('title', description);
		});

		// Add some dynamic styling to the gallery footer
		var themeColor = ($('.copyright a').lenght !== 0 ) ? $('.copyright a').css('color') : '#333333';
		$('<style>.chocolat-wrapper .chocolat-bottom{background:' + themeColor + ';}}</style>').appendTo('head');
		
		// Inititate
		$gallery.Chocolat({imageSelector: '.gallery-icon a', loop: true});
	});

})(jQuery);