# Lightbox only WP-gallery #

Plugin for [WordPress](http://wordpress.org) that only adds a lightbox to the galleries (and not all images). Using the responsive gallery js-lib, [Chocolat](chocolat.insipi.de). 

### Notes on multisite ###

This plugin uses a link in the footer of the site in order to create a dynamic styling. If it won't find a matching item it will fall back to #333333. A tip here is to just change the selector to your preferences. 

*Happy coding!*