<?php		
/*
Plugin Name: Lightbox only WP-gallery 
Description: Provides a lightbox functionality on <em>only</em> wordpress galleries using the responsive gallery js-lib, Chocolat. 
Author: Joakim Nyström	
Version: 1.0
Author URI: http://jnystromdesign.se
*/

function lowg_plugin_scripts(){
	// Add Chocolat
	wp_enqueue_script('chocolat-js',   plugins_url( '/chocolat/js/jquery.chocolat.min.js', __FILE__ ), array('jquery') );
    wp_enqueue_style('chocolat-style', plugins_url( '/chocolat/css/chocolat.css', __FILE__ ) );

    // Add some custom scripts
    wp_enqueue_script('lowg', plugins_url( '/js/lowg.js', __FILE__ ), array('jquery', 'chocolat-js') );
}
add_action( 'wp_enqueue_scripts', 'lowg_plugin_scripts' );
